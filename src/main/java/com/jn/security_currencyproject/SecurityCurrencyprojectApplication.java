package com.jn.security_currencyproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class SecurityCurrencyprojectApplication {

    public static void main(String[] args) {
        SpringApplication.run(SecurityCurrencyprojectApplication.class, args);
    }

}
