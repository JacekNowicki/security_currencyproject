package com.jn.security_currencyproject.controller;

import com.jn.security_currencyproject.model.AppUser;
import com.jn.security_currencyproject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(path = "/")
public class IndexController {

    @Autowired
    private UserService userService;

    @GetMapping(path = "/")
    public String index() {
        return "index";
    }

    @GetMapping("/login")
    public String loginForm() {
        return "loginForm";
    }

    @GetMapping("/register")
    public String registerForm(Model model){
        model.addAttribute("user", new AppUser());
        return "user/userRegisterForm";
    }

    @PostMapping("/register")
    public String registerUser(AppUser appUser, String passwordConfirm){
        userService.registerUser(appUser, passwordConfirm);
        return "redirect:/";
    }

    @GetMapping("/activate/{activation_id}")
    public String activate(@PathVariable("activation_id") String activationId,
                           Model model){
        model.addAttribute("success", userService.activate(activationId));
        return "user/activationResult";
    }
}
