package com.jn.security_currencyproject.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.internet.MimeMessage;

@Service
public class MailingService {
    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private TemplateEngine templateEngine;

    @Value("${spring.mail.username}")
    private String from;

    public void send(String emailAddress, String title, String content) throws Exception {
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage);
        helper.setTo(emailAddress);
        helper.setSubject(title);
        helper.setText(content, true);
        helper.setFrom(from);
        javaMailSender.send(mimeMessage);
    }

    public void sendActivationLink(String emailAddress, String activationLink) throws Exception {
        Context context = new Context();
        context.setVariable("activationLink", "http://localhost:8080/activate/" + activationLink);

        send(emailAddress, "Activate account!", loadEmail(context, "mailTemplates/activationTemplate"));
    }

    private String loadEmailFromTemplates(String templateName) {
        Context context = new Context();
        return loadEmail(context, templateName);
    }

    private String loadEmail(Context context, String templateName) {
        return templateEngine.process(templateName, context);
    }
}
