package com.jn.security_currencyproject.service;


import com.jn.security_currencyproject.exception.PasswordDoNotMatchException;
import com.jn.security_currencyproject.model.AppUser;
import com.jn.security_currencyproject.repository.AppUserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
public class UserServiceImpl implements UserService {
    private final static Logger log = LoggerFactory.getLogger(UserServiceImpl.class);
    private AppUserRepository appUserRepository;
    private BCryptPasswordEncoder passwordEncoder;
    private UserRoleService userRoleService;
    private MailingService mailingService;

    private final ExecutorService executorService = Executors.newSingleThreadExecutor();

    @Autowired
    public UserServiceImpl(AppUserRepository appUserRepository, BCryptPasswordEncoder passwordEncoder, UserRoleService userRoleService, MailingService mailingService) {
        this.appUserRepository = appUserRepository;
        this.passwordEncoder = passwordEncoder;
        this.userRoleService = userRoleService;
        this.mailingService = mailingService;
    }

    @Override
    public void registerUser(String username, String password, String passwordConfirm) {
        if (!password.equals(passwordConfirm)) {
            // coś jest nie tak - hasła się nie zgadzają
            throw new PasswordDoNotMatchException("Password and Password Confirm do not match.");
        }
        if (password.length() <= 3) {
            throw new PasswordDoNotMatchException("Password must be at least 4 characters.");
        }
        AppUser appUser = new AppUser();
        appUser.setEmail(username);
        appUser.setPassword(passwordEncoder.encode(password));

        // pobranie domyślnych uprawnień z userRoleService (który ładuje z konfiguracji)
        appUser.setRoles(userRoleService.getDefaultUserRoles());

        appUserRepository.save(appUser);
    }

    @Override
    public void registerUser(AppUser appUser, String passwordConfirm) {
        if (!appUser.getPassword().equals(passwordConfirm)) {
            // coś jest nie tak - hasła się nie zgadzają
            throw new PasswordDoNotMatchException("Password and Password Confirm do not match.");
        }
        if (appUser.getPassword().length() < 4) {
            throw new PasswordDoNotMatchException("Password must be at least 4 characters.");
        }

        do {
            log.info("Renewing activation code");
            appUser.setActivationCode(UUID.randomUUID().toString());
        } while (appUserRepository.findByActivationCode(appUser.getActivationCode()).isPresent());

        appUserRepository.save(appUser);
        executorService.submit(() -> {
            try {
                mailingService.sendActivationLink(appUser.getEmail(), appUser.getActivationCode());
            } catch (Exception e) {
                log.error("Error creating user", e);
            }
        });
    }

    @Override
    public List<AppUser> getAllUsers() {
        return appUserRepository.findAll();
    }

    @Override
    public boolean activate(String activationId) {
        Optional<AppUser> appUserOptional = appUserRepository.findByActivationCode(activationId);
        if (appUserOptional.isPresent()) {
            AppUser appUser = appUserOptional.get();

            appUser.setActivationCode(null);

            appUserRepository.save(appUser);

            return true;
        }

        return false;
    }
}
