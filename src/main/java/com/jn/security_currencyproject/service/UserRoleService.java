package com.jn.security_currencyproject.service;

import com.jn.security_currencyproject.model.UserRole;

import java.util.Set;

public interface UserRoleService {
    Set<UserRole> getDefaultUserRoles();
}
