package com.jn.security_currencyproject.service;


import com.jn.security_currencyproject.model.AppUser;

import java.util.List;

public interface UserService {
    void registerUser(String username, String password, String passwordConfirm);

    List<AppUser> getAllUsers();

    void registerUser(AppUser appUserm, String passwordConfirm);

    boolean activate(String activationId);
}
